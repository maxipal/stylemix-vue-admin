function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

function setCookie(name, value, date, domain = '/') {
  if(!date) date = 3600 * 24 * 30 // One month
  let expires = new Date(Date.now() + (date * 1000));
  document.cookie = `${name}=${value};expires=${expires};path=${domain}`;
}

function eraseCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getCookieExpire(name) {

}

export { getCookie, setCookie, eraseCookie };
