import config from './config'
import actions from './actions'

export default {
  actions,
  config,
}
