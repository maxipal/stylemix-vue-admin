export default {
  setLogoUrl({ commit }, logoUrl) {
    commit('setLogoUrl', logoUrl)
  },
}
