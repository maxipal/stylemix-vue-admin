/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from 'vue'
import { CHECK, LOGIN, LOGOUT } from './mutation-types'
import AuthConfig from '../config'
import { getCookie, setCookie, eraseCookie} from 'stylemix-vue-admin';

const prefix = AuthConfig.storagePrefix || 'app-console'

/* eslint-disable no-param-reassign */
export default {
  [CHECK](state) {
    const token = getCookie(`${prefix}_user-token`)
    state.token = token
    state.authenticated = !!token
    if (state.authenticated) {
      if (Vue.$http) {
        Vue.$http.defaults.headers.common.Authorization = `Bearer ${token}`
      }
    }
  },

  [LOGIN](state, { token, expiresIn, user }) {
    state.authenticated = true
    state.token = token
    setCookie(`${prefix}_user-token`, token, expiresIn)
    if(user && user.lang){
      setCookie(`${prefix}-language-code`, user.lang);
    }
    if (Vue.$http) {
      Vue.$http.defaults.headers.common.Authorization = `Bearer ${token}`
    }
  },

  [LOGOUT](state) {
    state.authenticated = false
    state.token = null
    eraseCookie(`${prefix}_user-token`)
    if (Vue.$http) {
      Vue.$http.defaults.headers.common.Authorization = ''
    }
  },

  account(state, account) {
    state.account = account
  },

  accountMeta(state, meta) {
    state.accountMeta = meta
  },

  attemptedRoute(state, attemptedRoute) {
    state.attemptedRoute = attemptedRoute
  },
}
