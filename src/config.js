const Config = {
  /**
   * Main callbacks for auth forms: login, register, forgot, logout
   */
  authHandler: null,

  /**
   * Default (home) route
   */
  defaultRoute: null,

  /**
   * Menu for sidebar
   */
  sidebarMenu: [],

  /**
   * Dropdown menu for account in navbar
   */
  accountMenu: [],
}

export default Config
