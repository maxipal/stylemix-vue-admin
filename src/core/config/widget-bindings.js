export default {
  string: 'widget-input',
  boolean: 'widget-checkbox',
  number: 'widget-input',
  text: 'widget-textarea',
  geolocation: 'widget-geolocation',
  datetime: 'widget-datetime',
}
